# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Go2NextFeatureDockWidget
                                 A QGIS plugin
 Allows jumping from a feature to another following an attribute order
                             -------------------
        begin                : 2016-12-27
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Alberto De Luca for Tabacco Editrice
        email                : info@tabaccoeditrice.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from builtins import str
import os

from collections import OrderedDict
from .geo_utils import utils as geo_utils, vector_utils as vutils
from qgis.gui import QgsMapLayerComboBox, QgsFieldComboBox, QgsMessageBar
from qgis.core import QgsProject, Qgis, QgsMapLayerProxyModel
from qgis.PyQt import uic
from qgis.PyQt.QtCore import pyqtSignal, Qt
from qgis.PyQt.QtWidgets import QVBoxLayout, QFrame, QFormLayout, QLabel, QLineEdit, QHBoxLayout,\
    QRadioButton, QButtonGroup, QCheckBox, QPushButton, QShortcut, QDockWidget, QSizePolicy, QSpacerItem
from qgis.PyQt.QtGui import QKeySequence, QCursor

from . import buttons_utils
import operator

try:
    from qgis.core import QgsMapLayerType
except:
    pass

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'go2nextfeature_dockwidget.ui'))


class Go2NextFeatureDockWidget(QDockWidget, FORM_CLASS):

    closingPlugin = pyqtSignal()
    plugin_name = 'Go2NextFeature3 2.13'

    def __init__(self, iface, parent=None):
        """Constructor."""
        super(Go2NextFeatureDockWidget, self).__init__(parent)
        self.setupUi(self)
        self.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)

        self.iface = iface

        self.feats_od = OrderedDict()
        self.ft_pos = -1
        self.sel_ft_pos = -1
        self.sel_ft_ids = None

        self.tool = None

        self.fra_main.setLineWidth(3)
        fra_main_layo = QVBoxLayout(self.fra_main)
        fra_main_layo.setContentsMargins(0, 0, 0, 0)
        fra_main_layo.setSpacing(0)

        # frame 1
        self.fra_1 = QFrame(self.fra_main)
        self.fra_1.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        fra_1_lay = QFormLayout(self.fra_1)

        self.lbl_layer = QLabel('Layer:')
        self.cbo_layer = QgsMapLayerComboBox()
        self.cbo_layer.setFilters(QgsMapLayerProxyModel.VectorLayer)
        self.cbo_layer.setAllowEmptyLayer(True)
        self.cbo_layer.setMinimumContentsLength(10)
        self.cbo_layer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        fra_1_lay.addRow(self.lbl_layer, self.cbo_layer)

        self.lbl_attribute = QLabel('Attribute:')
        self.cbo_attribute = QgsFieldComboBox()
        self.cbo_attribute.setMinimumContentsLength(10)
        self.cbo_attribute.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        fra_1_lay.addRow(self.lbl_attribute, self.cbo_attribute)

        self.lbl_curr = QLabel('Value:')
        self.txt_curr = QLineEdit()
        self.txt_curr.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.txt_curr.setReadOnly(True)
        fra_1_lay.addRow(self.lbl_curr, self.txt_curr)

        self.lbl_ftCount1 = QLabel("Count:")
        self.lbl_ftCount = QLabel("- of - features")
        fra_1_lay.addRow(self.lbl_ftCount1, self.lbl_ftCount)

        self.lbl_action = QLabel('Action')
        self.fra_action = QFrame(self.fra_1)
        fra_action_lay = QHBoxLayout(self.fra_action)
        fra_action_lay.setContentsMargins(5, 0, 5, 0)
        self.rad_action_pan = QRadioButton('Pan')
        self.rad_action_pan.setChecked(True)
        self.rad_action_zoom = QRadioButton('Zoom')

        self.gra_action = QButtonGroup(self.fra_1)
        self.gra_action.addButton(self.rad_action_pan)
        self.gra_action.addButton(self.rad_action_zoom)

        fra_action_lay.addWidget(self.rad_action_pan)
        fra_action_lay.addWidget(self.rad_action_zoom)
        fra_1_lay.addRow(self.lbl_action, self.fra_action)

        # frame 2
        self.fra_2 = QFrame(self.fra_main)
        fra_2_lay = QVBoxLayout(self.fra_2)
        fra_2_lay.setContentsMargins(5, 0, 10, 0)

        self.chk_use_sel = QCheckBox('Go to selected features only')
        fra_2_lay.addWidget(self.chk_use_sel)

        self.chk_start_sel = QCheckBox('Start from selected feature')
        fra_2_lay.addWidget(self.chk_start_sel)

        self.chk_select = QCheckBox('Select')

        fra_2_lay.addWidget(self.chk_select)

        # frame 3
        self.fra_3 = QFrame(self.fra_main)
        fra_3_lay = QHBoxLayout(self.fra_3)
        fra_3_lay.setContentsMargins(5, 5, 10, 10)

        self.btn_prev = QPushButton(u'\u25C0\u25C0')
        self.btn_next = QPushButton(u'\u25B6\u25B6')

        fra_3_lay.addWidget(self.btn_prev)
        fra_3_lay.addWidget(self.btn_next)
        self.btn_prev.resize(11, 100)
        self.btn_next.resize(11, 100)

        # Add everything to main frame
        fra_main_layo.addWidget(self.fra_1)
        fra_main_layo.addWidget(self.fra_2)
        fra_main_layo.addWidget(self.fra_3)

        spacer_item = QSpacerItem(0, 1000, QSizePolicy.Expanding, QSizePolicy.Expanding)
        fra_main_layo.addItem(spacer_item)

        self.setup()

    def setup(self):
        self.setWindowTitle(Go2NextFeatureDockWidget.plugin_name)
        # self.chk_select.setEnabled(False)

        self.cbo_layer.setCurrentIndex(0)

        # QgsProject.instance().layerWasAdded.connect(self.update_layers_combos)
        # QgsProject.instance().layerRemoved.connect(self.update_layers_combos)

        self.chk_start_sel.toggled.connect(self.chk_start_sel_clicked)
        self.chk_use_sel.toggled.connect(self.chk_use_sel_clicked)

        # self.cbo_layer.activated.connect(self.cbo_layer_activated)
        # self.cbo_attribute.activated.connect(self.cbo_attrib_activated)

        self.cbo_layer.layerChanged.connect(self.cbo_attribute.setLayer)  # setLayer is a native slot function
        self.cbo_layer.layerChanged.connect(self.cbo_layer_layerchanged)
        self.cbo_attribute.fieldChanged.connect(self.cbo_attrib_activated)

        # self.cbo_layer.layerChanged.connect(self.cbo_layer_activated)

        self.btn_prev.pressed.connect(self.btn_prev_pressed)
        self.btn_next.pressed.connect(self.btn_next_pressed)

        # self.update_layers_combos()

        # Turn off controls
        # self.cbo_attribute.setEnabled(False)
        self.chk_use_sel.setEnabled(False)
        self.chk_start_sel.setEnabled(False)
        self.chk_select.setEnabled(False)
        self.btn_prev.setEnabled(False)
        self.btn_next.setEnabled(False)

        # Shortcut
        shortcut = QShortcut(QKeySequence(Qt.Key_F8), self.iface.mainWindow())
        shortcut.setContext(Qt.ApplicationShortcut)
        shortcut.activated.connect(self.btn_next_pressed)

    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()

    # def update_layers_combos(self):
    #
    #         prev_lay_id = None
    #         if self.cbo_layer.currentIndex() > -1:
    #             prev_lay_id = self.cbo_layer.itemData(self.cbo_layer.currentIndex())
    #
    #         # Find layers IDs and names
    #         layer_ids = QgsProject.instance().mapLayers()
    #         names_ids_d = {}
    #         for layer_id in layer_ids:
    #             layer = geo_utils.LayerUtils.get_lay_from_id(layer_id)
    #             if layer is not None:
    #                 if Qgis.QGIS_VERSION_INT < 30800:
    #                     if layer.type() == 0:
    #                          names_ids_d[layer_id] = layer.name()
    #                 else:
    #                     if layer.type() == QgsMapLayerType.VectorLayer:
    #                         names_ids_d[layer_id] = layer.name()
    #
    #         # Fill combo
    #         self.cbo_layer.clear()
    #         self.cbo_layer.addItem('', None)
    #
    #         sorted_ids_names = sorted(list(names_ids_d.items()), key=operator.itemgetter(1))
    #
    #         for name_id in sorted_ids_names:
    #             self.cbo_layer.addItem(name_id[1], name_id[0])
    #
    #         if prev_lay_id is not None:
    #             self.lay_id = self.set_layercombo_index(self.cbo_layer, prev_lay_id)
    #
    #         else:
    #
    #             self.btn_prev.setEnabled(self.lay_id is not None)
    #             self.btn_next.setEnabled(self.lay_id is not None)
    #             self.cbo_layer_activated()
    #
    #             if not self.lay_id:
    #                 self.txt_curr.setText('-')
    #                 self.lbl_ftCount.setText("- of %d features" % (len(self.feats_od)))

    def chk_start_sel_clicked(self):

        if self.chk_start_sel.isChecked():
            self.sel_ft_ids = self.cbo_layer.currentLayer().selectedFeatureIds()
            if len(self.sel_ft_ids) != 1:
                self.iface.messageBar().pushMessage(
                    Go2NextFeatureDockWidget.plugin_name,
                    'Please select one feature.',
                    Qgis.Warning)  # TODO: softcode
                return

            self.ft_pos = -1
            self.sel_ft_pos = -1

            # Reset buttons
            self.btn_prev.setEnabled(False)
            self.btn_next.setEnabled(True)

    def chk_use_sel_clicked(self):

        self.sel_ft_ids = self.cbo_layer.currentLayer().selectedFeatureIds()
        if self.chk_use_sel.isChecked() and len(self.sel_ft_ids) < 1:
            self.iface.messageBar().pushMessage(
                Go2NextFeatureDockWidget.plugin_name,
                'Please select at least one feature.',
                Qgis.Warning)  # TODO: softcode
            return

        self.ft_pos = -1
        self.sel_ft_pos = -1
        self.chk_start_sel.setEnabled(not self.chk_use_sel.isChecked())
        self.chk_select.setEnabled(not self.chk_use_sel.isChecked())

        # Reset buttons
        self.btn_prev.setEnabled(False)
        self.btn_next.setEnabled(True)

    def cbo_layer_activated(self):

        self.feats_od.clear()
        self.txt_curr.setText('-')
        self.lbl_ftCount.setText("- of %d features" % (len(self.feats_od)))

    def lay_selection_changed(self):
        self.chk_use_sel.setChecked(False)

    def cbo_attrib_activated(self):

        if self.cbo_layer.currentLayer() is None or self.cbo_layer.currentLayer() == '':
            return

        if self.cbo_attribute.currentField() is None or self.cbo_attribute.currentField() == '':
            return

        self.feats_od.clear()

        feats_d = {}

        for feat in self.cbo_layer.currentLayer().getFeatures():
            feats_d[feat] = feat.attribute(self.cbo_attribute.currentField())
            # feats_d[feat.id()] = feat.attribute(self.cbo_attribute.currentField())

        # Sort features by attribute
        feats_d_sorted = sorted(feats_d.items(), key=lambda x: x[1])
        # feats_d_s = [(k, feats_d[k]) for k in sorted(feats_d, key=feats_d.get, reverse=False)]

        pos = 0
        self.feats_od.clear()
        for k, v in feats_d_sorted:
            self.feats_od[pos] = k
            pos += 1

        self.ft_pos = -1

        self.chk_use_sel.setEnabled(True)
        self.chk_start_sel.setEnabled(True)
        self.cbo_attribute.setEnabled(True)
        self.chk_select.setEnabled(True)
        self.btn_prev.setEnabled(False)
        self.btn_next.setEnabled(True)

        self.txt_curr.setText('-')
        self.lbl_ftCount.setText("- of %d features" % (len(self.feats_od)))

    def btn_prev_pressed(self):

        # self.ft_pos -= 1
        self.next_ft(-1)

        self.lbl_ftCount.setText("%d of %d features" % ((self.ft_pos + 1), len(self.feats_od)))

        self.btn_next.setEnabled(True)
        if self.ft_pos <= 0:
            self.btn_prev.setEnabled(False)

    def btn_next_pressed(self):

        # self.ft_pos += 1
        self.next_ft(1)

        self.lbl_ftCount.setText("%d of %d features" % ((self.ft_pos + 1), len(self.feats_od)))

        if self.ft_pos >= len(self.feats_od) - 1:
            self.btn_next.setEnabled(False)

        if len(self.feats_od) > 1 and self.ft_pos > 0:
            self.btn_prev.setEnabled(True)

    def next_ft(self, increment):

        self.ft_pos += increment

        self.ft_pos = max(self.ft_pos, 0)
        self.sel_ft_pos = max(self.ft_pos, 0)
        self.ft_pos = min(self.ft_pos, len(self.feats_od) - 1)
        self.sel_ft_pos = min(self.ft_pos, len(self.feats_od) - 1)

        if self.chk_use_sel.isChecked():

            while not self.feats_od[self.ft_pos].id() in self.sel_ft_ids:
                self.ft_pos += increment
                if self.ft_pos >= len(self.feats_od) - 1 or self.ft_pos <= 0:
                    self.ft_pos -= increment
                    return

            self.sel_ft_pos += increment
            if self.sel_ft_pos == len(self.sel_ft_ids):
                self.btn_next.setEnabled(False)
            if self.sel_ft_pos == 0:
                self.btn_prev.setEnabled(False)

        if self.chk_start_sel.isChecked():

            sel_ft_ids = self.cbo_layer.currentLayer().selectedFeatureIds()
            if len(sel_ft_ids) != 1:
                self.iface.messageBar().pushMessage(
                    Go2NextFeatureDockWidget.plugin_name,
                    'Please select only one feature.',
                    Qgis.Warning)  # TODO: softcode
                return

            sel_ft_id = sel_ft_ids[0]
            sel_ft_index = -1
            for index, feat in self.feats_od.items():
                if feat.id() == sel_ft_id:
                    sel_ft_index = index
                    break

            if sel_ft_index != -1:
                self.ft_pos = sel_ft_index
                self.chk_start_sel.setChecked(False)

        if 0 <= self.ft_pos < len(self.feats_od):

            renderer = self.iface.mapCanvas().mapSettings()
            geom = self.feats_od[self.ft_pos].geometry()

            if geom.isNull():
                self.iface.messageBar().pushInfo(
                    Go2NextFeatureDockWidget.plugin_name,
                    'The geometry of the feature is null: can neither zoom nor pan to it.')  # TODO: softcode

            else:

                if self.rad_action_pan.isChecked():
                    self.iface.mapCanvas().setCenter(renderer.layerToMapCoordinates(
                        self.cbo_layer.currentLayer(),
                        geom.centroid().asPoint()))

                elif self.rad_action_zoom.isChecked():
                    self.iface.mapCanvas().setExtent(renderer.layerToMapCoordinates(
                        self.cbo_layer.currentLayer(),
                        geom.boundingBox()))
                    self.iface.mapCanvas().zoomByFactor(1.1)

            if self.chk_select.isChecked():

                if Qgis.QGIS_VERSION_INT >= 21600:
                    self.cbo_layer.currentLayer().selectByIds([self.feats_od[self.ft_pos].id()])
                else:
                    self.cbo_layer.currentLayer().setSelectedFeatures([self.feats_od[self.ft_pos].id()])

            self.iface.mapCanvas().refresh()

            attrib_value = self.feats_od[self.ft_pos].attribute(self.cbo_attribute.currentField())

            self.txt_curr.setText(str(attrib_value))
            # self.txt_curr.setText(str(self.feats_od[self.ft_pos].id()))

    def cbo_layer_layerchanged(self):

        self.cbo_attrib_activated()




    # def set_layercombo_index(self, combo, layer_id):
    #
    #     index = combo.findData(layer_id)
    #     if index >= 0:
    #         combo.setCurrentIndex(index)
    #         return geo_utils.LayerUtils.get_lay_from_id(self.get_combo_current_data(combo))
    #     else:
    #         if combo.count() > 0:
    #             combo.setCurrentIndex(0)
    #             return combo.itemData(0)
    #         else:
    #             return None

    # def get_combo_current_data(self, combo):
    #
    #     index = combo.currentIndex()
    #     return combo.itemData(index)

    # def set_cursor(self, cursor_shape):
    #     cursor = QCursor()
    #     cursor.setShape(cursor_shape)
    #     self.iface.mapCanvas().setCursor(cursor)

